import os
import struct
import math
from vtf_data import *
from enum import Enum
from operator import add
import matplotlib.pyplot as plt
import numpy as np

dxn_block = \
    [[0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0]]

def linear_interp(start, end, steps):
    ''' 
        Given two values start and end, return a list of
        steps (incl. start and end) length of linearly-interpolated 
        values between the two. If start and end are lists 
        representing colors, return a list of linearly-interpolated colors
        ex: linear_interp(0, 10, 6) = [0.0, 2.0, 4.0, 6.0, 8.0, 10.0]
    '''

    out = []

    if type(start) != type(end):
        return

    is_color = type(start) == list

    for i in range(steps):
        factor_start = i / (steps-1)
        factor_end = 1 - (i / (steps-1))
        value = list(map(add, [x * factor_start for x in start], [x * factor_end for x in end])) \
            if is_color \
            else start * factor_start + end * factor_end

        out.append(value)
    
    return list(reversed(out))

# Parse a 16-bit-long 5-6-5 encoded RGB color
def color_565(bits):
    if len(bits) != 16:
        return
    
    # shift each to the left to reach 8 bits, 
    # then divide by 2 ** 8 = 256 to get a [0..1] RGB value
    r = (int(bits[:5], 2) << 3) / 256
    g = (int(bits[5:11], 2) << 2) / 256
    b = (int(bits[11:], 2) << 3) / 256

    # Return as [red, green, blue, alpha] where each value ranges from 0 to 1
    return [r, g, b, 1]


def bits_from_int(num):
    ''' Convert an integer to a list of bits '''
    return [int(char) for char in '{0:08b}'.format(num)]

def bits_from_bytes(bytes):
    ''' Convert a list of bytes to a string of bits, represented as integer 0s and 1s '''
    return "".join([format(byte, '08b') for byte in bytes])

def insert_block(cont, block, maxwidth):
    '''Insert a 2D array block into a container 2D array.
    Container is resized to make room for the block.
    Blocks are tiled horizontally until they reach maxwidth,
    and then they are inserted on a new row'''

    next_available_row = 0
    block_size = len(block)

    while (True):
        if (next_available_row >= len(cont)):
            cont += [[]] * block_size
            break
        
        if (len(cont[next_available_row]) >= maxwidth):
            next_available_row += 1
        else:
            break

    if next_available_row % block_size != 0: # something's wrong
        return

    for i in range(block_size):
        cont[next_available_row + i] = cont[next_available_row + i] + block[i]

class VTF:
    def __init__(self):
        self.raw = ""
        self.filename = ""
        self.header = {}

        # fallback values, override these once we've read the header
        # (technically header_size shouldn't change)
        self.header_size = HEADER_SIZE
        self.low_res_image_data_size = 128 


    def parse_flags(self, bits):
        ''' Parse a list of bits representing the 4-byte flags field
            Use the hard-coded header_flags data variable in vtf_data '''

        bits_len = len(bits)
        head_len = len(header_flags)

        if bits_len > head_len:
            return
        elif bits_len < head_len:
            bits = [0] * (head_len - bits_len) + bits

        flags = {}
        # Reverse header_flags because first flags are stored in smallest bits (i.e. the end of the number)
        for ind, flag in enumerate(header_flags[::-1]): 
            flags[flag] = bool(bits[ind])

        return flags
    

    def create_struct_format(self):
        ''' Convert the header_format header representation
        in vtf_data to a string that the Python struct module
        can understand '''

        type_map = {
            Data.CHAR: 'c',
            Data.UCHAR: 'B',
            Data.INT: 'i',
            Data.UINT: 'I',
            Data.SHORT: 'h',
            Data.USHORT: 'H',
            Data.FLOAT: 'f'
        }
        format_str = "="
        total_count = 0

        for tup in header_format:
            (_, data_type, count) = tup
            add = type_map[data_type]

            if count > 1:
                add = str(count) + add
            
            format_str += add
            total_count += count * data_size[data_type]
        
        # The size of the actual data in the header is closer to 65 bytes
        # so we pad it with zeroes to reach the full 80
        pad = HEADER_SIZE - total_count
        return format_str + ("0{}c".format(pad) if pad > 0 else "")
    
    def model_struct_output(self, output):
        ''' Create a useful dict representation with descriptive keys of the values that
        we unpack from the raw binary data with the struct module, using the pre-defined
        header_format variable in vtf_data '''
        index = 0
        model = {}

        for tup in header_format:
            (name, data_type, count) = tup
            model[name] = output[index] if count == 1 else output[index:index+count]
            index += count

        model["flags"] = self.parse_flags(bits_from_int(model["flags"]))
        return model

    def construct_header(self):
        ''' Construct the header object and store it in memory
        Create the struct format string, unpack the binary data,
        model the data, and perform integrity checks '''

        format_str = self.create_struct_format()

        data = struct.unpack(format_str, self.raw[:HEADER_SIZE])

        model = self.model_struct_output(data)

        # VTF integrity checks
        if model["signature"] != (b'V', b'T', b'F', b'\x00') \
            or model["low_res_image_format"] != 13:
            raise Exception("Invalid VTF file given")

        if model["header_size"] != HEADER_SIZE:
            raise Exception("Unexpected header size")
        
        self.header_size = model["header_size"]
        self.low_res_image_data_size = int((model["low_res_image_width"] * model["low_res_image_height"]) / 16 * 8)

        self.header = model
        
    def decode_dxt1(self, bytes, dim):
        ''' DXT1 summary:
            - Image is divided up into 4x4-pixel blocks. Each block is assigned 4 colors. 64 bits per block.
            - For each 4x4 block, two colors are stored (two other colors are interpolated 
                between these two, for a total of four), plus a 4x4 2-bit lookup table 
                indicating which of the 4 colors each pixel has
            - First two bytes (16 bits) describe color #1, using 5-6-5 bit storage for R-G-B values, respectively
            - Next two bytes store the second color in the same exact way (32 bits for both colors)
            - Last 32 bits are used for the lookup table ((4 x 4) * (2 bits)) = 32
            - However, there is also a way to specify a 1-bit alpha channel. If, when interpreted as
                16-bit unsigned integers, the first color has a lesser numerical value than the second
                color, the alpha "switch" is flipped. Then, there are only three colors 
                (2 specified + 1 interpolated), and the fourth lookup value (11 in binary) 
                is mapped to the alpha channel

            This function decodes a range of bytes representing a DXT1-encoded image
            using the rules stated above
        '''

        # DXT1 image size must be a multiple of 64 bits
        if len(bytes) % 8 != 0:
            raise Exception("Invalid DXT1 data size (must be a multiple of 8 bytes)")
        
        
        output = []
        for i in range(0, len(bytes), 8):
            in_block = bytes[i:i+8]
            out_block = dxn_block.copy()

            c1_bits = bits_from_bytes(in_block[0:2])
            c2_bits = bits_from_bytes(in_block[2:4])

            # Interpret the two colors as 16-bit unsigned integers
            # If the first is less than the second, there is an alpha channel
            alpha_check = int(c1_bits, 2) < int(c2_bits, 2)

            colors = linear_interp(color_565(c1_bits), color_565(c2_bits), 3) \
                if alpha_check \
                else linear_interp(color_565(c1_bits), color_565(c2_bits), 4)

            '''
                Lookup table (LSB aka reversed rows):
                Each cell of the table is two bits
                00 = color 1, 01 = color 2, 10 = color 3 (interpolated), 
                11 = color 4 (interpolated) if alpha not enabled, else 11 = opaque/transparent 

                 x1  x2  x3  x4
                - - - - - - - - -
            y1  | d | c | b | a |
                - - - - - - - - -
            y2  | h | g | f | e |
                - - - - - - - - -
            y3  | l | k | j | i |
                - - - - - - - - -
            y4  | p | o | n | m |
                - - - - - - - - -
            '''
            lookup_table = bits_from_bytes(in_block[4:8])


            for j in range(0, len(lookup_table), 2):
                y_ind = math.floor(j / 8) # Each row of lookup table is 8 bits long

                # Divide index modulo table width by 2 to get column index
                # Subtract from block width (4 minus 1 = 3 because 0 indexing) 
                # to account for reversed rows
                x_ind = 3 - math.floor((j % 8) / 2)

                value = int("".join(lookup_table[j:j+2]), 2)

                if alpha_check and value == 3:
                    out_block[y_ind][x_ind] = [0, 0, 0, 0]
                else:
                    out_block[y_ind][x_ind] = colors[value]

            insert_block(output, out_block, dim[0])

        return output

    def decode_dxt5(self, bytes, dim):
        ''' DXT5 summary:
            - Similar to DXT1, but double the space, because additional reference values
                and an additional lookup table are stored just for alpha values
            - Two alpha values are stored in the first two bytes, and 6 more are interpolated
                between these two for a total of 8 alpha values. This means 3 bits must
                be used per cell of the lookup table (2^3=8)
            - The next 6 bytes are used for the alpha lookup table (16 pixels * 3 bits per
                = 48 bits / 8 = 6 bytes)
            - After this, the rest of DXT5 is the same as DXT1 (4 subsequent bytes for two colors,
                then 4 bytes for the 2-bit color lookup table)
        '''
        if len(bytes) % 16 != 0:
            raise Exception("Invalid DXT5 data size (must be a multiple of 16 bytes)")

        alpha_output = []
        color_output = []

        for i in range(0, len(bytes), 16):
            alpha_block = bytes[i:i+8]
            color_block = bytes[i+8:i+16]

            # Do alpha first
            alpha_value_start = int(bits_from_bytes([alpha_block[0]]), 2)
            alpha_value_end = int(bits_from_bytes([alpha_block[1]]), 2)

            alpha_values = linear_interp(alpha_value_start, alpha_value_end, 8)
            alpha_lookup_table = bits_from_bytes(alpha_block[2:])

            alpha_out_block = dxn_block.copy()

            for j in range(0, len(alpha_lookup_table), 3):
                y_ind = math.floor(j / 12)
                x_ind = 3 - math.floor((j % 12) / 3)
            

                value = int("".join(alpha_lookup_table[j:j+3]), 2)

                alpha_out_block[y_ind][x_ind] = alpha_values[value] / (2 ** 8)

            insert_block(alpha_output, alpha_out_block, dim[0])


            # Do colors
            c1 = color_565(bits_from_bytes(color_block[0:2]))
            c2 = color_565(bits_from_bytes(color_block[2:4]))

            color_values = linear_interp(c1, c2, 4)

            color_lookup_table = bits_from_bytes(color_block[4:])
            color_out_block = dxn_block.copy()


            for j in range(0, len(color_lookup_table), 2):
                y_ind = math.floor(j / 8)
                x_ind = 3 - math.floor((j % 8) / 2)

                value = int(
                    "".join(color_lookup_table[j:j+2]), 2)
                color_out_block[y_ind][x_ind] = color_values[value]
            
            insert_block(color_output, color_out_block, dim[0])
            
        if len(alpha_output) != len(color_output) \
            or len(alpha_output[0]) != len(color_output[0]): # something went wrong
            return

        output = []

        for i, row in enumerate(color_output):
            output.append([])
            for j, col in enumerate(row):
                output[i].append(color_output[i][j][:3] + [alpha_output[i][j]])

        return output


        
    def read_low_res_image_data(self):
        if not self.header \
            or not self.header["low_res_image_format"] \
            or not self.header["low_res_image_width"] \
            or not self.header["low_res_image_height"]:
                raise Exception("Valid header must be loaded before low res image data can be loaded")
            
        dim = [
            self.header["low_res_image_width"],
            self.header["low_res_image_height"]
        ]

        if dim[0] % 4 != 0 or dim[1] % 4 != 0:
            raise Exception("Invalid low res image dimensions given in header")
        
        # Number of blocks = (area of picture / area of block) = (w_pic * h_pic) / (w_block * h_block)
        # Bytes per block = 8 (64 bits / 8 bits per byte)
        # Total byte length of low res image = Number of blocks * bytes per block
        byte_length = int((dim[0] * dim[1]) / (4 * 4)) * 8
        start_byte = self.header_size
        self.low_res_image = self.decode_dxt1(self.raw[start_byte:start_byte + byte_length], dim)

    def read_high_res_image_data(self):
        '''
        Decode the highest resolution of the image available
        Since there are likely mipmaps, and mipmaps are stored
        from smallest to largest, we need to find the beginning 
        of the last mipmap and then decode that
        '''

        w = self.header["width"]
        h = self.header["height"]
        mm_count = self.header["mipmap_count"]

        start_byte = self.header_size + self.low_res_image_data_size

        # mipmap check
        if min(w, h) / 2 ** (mm_count - 1) < 1:
            raise Exception("Too many mipmaps for size of image")

        # DXn algorithms seem to pad out mipmaps < 4x4 pixels so that they also take up 16 bytes
        # Create a list with all of the mipmap byte sizes. For the small ones, default to 16
        # For a 64x64 vtf, mipmap_bytes = [16, 16, 16, 64, 256, 1024, 4096]
        mipmap_bytes = [ int(max((w / (2 ** i)) * (h / (2 ** i)), 16.0)) for i in range(mm_count - 1, -1, -1) ]

        # Because DXT5 stores 16 pixels in 16 bytes, # of pixels = # of bytes
        last_mipmap_start = start_byte + int(sum(mipmap_bytes[:-1]))
        last_mipmap_bytes = int(mipmap_bytes[-1])

        # Finding the last mipmap by scanning forward should be the same
        # as subtracting its byte size from the size of the whole file
        if len(self.raw) - last_mipmap_bytes != last_mipmap_start:
            raise Exception("Unknown extra data at end of file")

        self.high_res_image = self.decode_dxt5(self.raw[last_mipmap_start : last_mipmap_start + last_mipmap_bytes], [w,h])
        return self.high_res_image
        

    def decode(self, arg):
        '''
            Will attempt to decode VTF information
            If a string is passed, it will treat it as a file name and try to read the file
            If a bytes object is passed, it will try to decode the bytes directly
        '''

        if type(arg) == str:
            self.filename = arg
            with open(arg, 'rb') as f:
                self.raw = f.read()
        elif type(arg) == bytes:
            self.raw = arg
        else:
            raise Exception("Invalid argument given to decode.")

        self.construct_header()
        self.read_low_res_image_data()
        return self.read_high_res_image_data()


if __name__ == "__main__":
    print(linear_interp([0,4,0,3], [6,1,2,1], 4))
    test = VTF()
    hr = test.decode("seekerOL.vtf")
    lr = test.low_res_image

    a = test.raw[208:]
    for i in range(0, len(list(a)), 16):
        print(list(a)[i:i+16])

    plt.imshow(lr)
    plt.show()