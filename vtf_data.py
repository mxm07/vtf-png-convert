
from enum import Enum

HEADER_SIZE = 80

# A selection of C data types and their respective sizes
class Data(Enum):
    CHAR = 0
    UCHAR = 1
    INT = 2
    UINT = 3
    SHORT = 4
    USHORT = 5
    FLOAT = 6

data_size = {
    Data.CHAR: 1,
    Data.UCHAR: 1,
    Data.INT: 4,
    Data.UINT: 4,
    Data.SHORT: 2,
    Data.USHORT: 2,
    Data.FLOAT: 4 
}


# VTF image formats, as described in VTFLib's source code
# Many of these won't be used
class IMAGE_FORMAT(Enum):
    NONE = -1
    RGBA8888 = 0
    ABGR8888 = 1
    RGB888 = 2
    BGR888 = 3
    RGB565 = 4
    I8 = 5
    IA88 = 6
    P8 = 7
    A8 = 8
    RGB888_BLUESCREEN = 9
    BGR888_BLUESCREEN = 10
    ARGB8888 = 11
    BGRA8888 = 12
    DXT1 = 13
    DXT3 = 14
    DXT5 = 15
    BGRX8888 = 16
    BGR565 = 17
    BGRX5551 = 18
    BGRA4444 = 19
    DXT1_ONEBITALPHA = 20
    BGRA5551 = 21
    UV88 = 22
    UVWQ8888 = 23
    RGBA16161616F = 24
    RGBA16161616 = 25
    UVLX8888 = 26

# Format of a VTF header, emulating the original C-struct
# Each line is (identifier, data type, count)
# count = the length of the array, or 1 if not an array
header_format = [
    ("signature", Data.CHAR, 4),
    ("version", Data.UINT, 2),
    ("header_size", Data.UINT, 1),
    ("width", Data.USHORT, 1),
    ("height", Data.USHORT, 1),
    ("flags", Data.UINT, 1),
    ("frames", Data.USHORT, 1),
    ("firstFrame", Data.USHORT, 1),
    ("padding0", Data.UCHAR, 4),
    ("reflectivity", Data.FLOAT, 3),
    ("padding1", Data.UCHAR, 4),
    ("bumpmap_scale", Data.FLOAT, 1),
    ("high_res_image_format", Data.UINT, 1),
    ("mipmap_count", Data.UCHAR, 1),
    ("low_res_image_format", Data.UINT, 1),
    ("low_res_image_width", Data.UCHAR, 1),
    ("low_res_image_height", Data.UCHAR, 1),
    ("depth", Data.USHORT, 1)
]

# Header flags that the VTF header field "flags" uses
# The flag corresponds with its index in the list
# flags are stored as a 4-byte unsigned int, and 
# "point_sample" corresponds to the first value which is 0x00000001,
# all the way up to "border", which corresponds to 0x20000000
header_flags = [
    "point_sample",
    "trilinear",
    "clamp_s",
    "clamp_t",
    "anisotropic",
    "hint_dxt5",
    "pwl_corrected",
    "normal",
    "nomip",
    "nolod",
    "all_mips",
    "procedural",
    "onebitalpha",
    "eightbitalpha",
    "envmap",
    "rendertarget",
    "depthrendertarget",
    "nodebugoverride",
    "singlecopy",
    "pre_srgb",
    "unused_1",
    "unused_2",
    "unused_3",
    "no_depth_buffer",
    "unused_4",
    "clamp_u",
    "vertex_texture",
    "ss_bump",
    "unused_5",
    "border",
    "unused_6",
    "unused_7"
]
